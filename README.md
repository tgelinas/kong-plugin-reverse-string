**Kong Reverse String Custom Plugin**

Description: This plugin will reverse the string of the response body by whitespace

Uses https://github.com/Kong/kong-plugin as a template and https://github.com/Kong/kong-vagrant as a development environment.
